<?php

/*
 * This file is part of the Scribe Mantle Bundle.
 *
 * (c) Scribe Inc. <source@scribe.software>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Scribe\Utility\Serializer;

use Scribe\Utility\StaticClass\StaticClassTrait;

/**
 * Class Serializer.
 */
class Serializer extends AbstractSerializer
{
    /*
     * Trait to disallow class instantiation
     */
    use StaticClassTrait;
}

/* EOF */
