# Scribe / Mantle Bundle

[![Travis](https://img.shields.io/travis/scribenet/symfony-mantle-bundle/master.svg?style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/ci) 
[![Scrutinizer](https://img.shields.io/scrutinizer/g/scribenet/symfony-mantle-bundle/master.svg?style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/quality)
[![Coveralls](https://img.shields.io/coveralls/scribenet/symfony-mantle-bundle/master.svg?style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/coverage)
[![Gemnasium](https://img.shields.io/gemnasium/scribenet/symfony-mantle-bundle.svg?style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/deps)

*Scribe / Mantle Bundle* is an MIT licensed internal dependency used by
Scribe Inc for our public and internal website applications as well as our client
web projects.

## License, Usage, Documentation

[![License](https://img.shields.io/badge/license-MIT-008ac6.svg?style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/license)
[![RTD](https://readthedocs.org/projects/symfony-mantle-bundle/badge/?version=latest&style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/docs)
[![API](https://img.shields.io/badge/docs-reference%20api-c75ec1.svg?style=flat-square)](https://symfony-mantle-bundle.docs.scribe.tools/api)

This project is licensed under the [MIT License](https://symfony-mantle-bundle.docs.scribe.tools/license).
User documentation as well as API reference is available via the above buttons.

**Visit our [Read the Docs page](https://symfony-mantle-bundle.docs.scribe.tools/docs) for additional documentation.**
